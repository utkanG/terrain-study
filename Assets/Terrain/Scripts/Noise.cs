using UnityEngine;
using Random = System.Random;

public static class Noise
{
    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offset)
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        Random randomFromSeed = new Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];
        for (int i = 0; i < octaves; i++)
        {
            float offsetX = randomFromSeed.Next(-100000, 100000);
            float offsetY = randomFromSeed.Next(-100000, 100000);
            octaveOffsets[i] = new Vector2(offsetX, -offsetY) + offset;
        }

        if (scale <= 0)
        {
            scale = float.Epsilon;
        }

        float minNoiseHeight = float.MaxValue;
        float maxNoiseHeight = float.MinValue;

        float halfWidth = mapWidth / 2f;
        float halfHeight = mapHeight / 2f;

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < octaves; i++)
                {
                    float sampleX = ((x - halfWidth) / scale + offset.x) * frequency;
                    float sampleY = ((y - halfHeight) / scale + offset.y) * frequency ;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;

                    amplitude *= persistence;
                    frequency *= lacunarity;
                }

                //if(noiseHeight > maxNoiseHeight)
                //{
                //    maxNoiseHeight = noiseHeight;
                //}
                //else if(noiseHeight < minNoiseHeight)
                //{
                //    minNoiseHeight = noiseHeight;
                //}

                noiseMap[x, y] = noiseHeight;
            }
        }

        for(int y = 0; y < mapHeight; y++)
        {
            for(int x = 0; x < mapWidth; x++)
            {
                //noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
            }
        }

        return noiseMap;
    }
}