using System;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator
{
    // Default generation with simpler mesh data.
    public static MeshData GenerateMeshData(float[,] heightMap, float heightCoefficient, AnimationCurve coefficientOverHeight)
    {
        int length = heightMap.GetLength(0);
        int width = heightMap.GetLength(1);
        float topLeftX = (length - 1) / -2f;
        float topLeftZ = (width - 1) / 2f;

        MeshData meshData = new MeshData(length, width);
        int vertexIndex = 0;

        for (int y = 0; y < width; y++)
        {
            for (int x = 0; x < length; x++)
            {
                float heightValue = heightCoefficient * (2 * coefficientOverHeight.Evaluate(heightMap[x, y]) - 1) + heightCoefficient;
                //float heightValue = heightCoefficient * coefficientOverHeight.Evaluate(heightMap[x, y]);
                meshData.Vertices[vertexIndex] = new Vector3(topLeftX + x, heightValue, topLeftZ - y);
                meshData.UVs[vertexIndex] = new Vector2(x/(float)length, y/(float)width);
                
                if (x < length - 1 && y < width - 1)
                {
                    meshData.AddTriangle(vertexIndex, vertexIndex + length + 1, vertexIndex + length);
                    meshData.AddTriangle(vertexIndex + length + 1, vertexIndex, vertexIndex + 1);
                }

                vertexIndex++;
            }
        }
        
        return meshData;
    }

    public static MeshData GenerateTerracedMeshData(MeshData meshData, float unitHeight)
    {
        // Create and edit new data while iterating
        List<Vector3> vertices = new List<Vector3>();
        List<int> triangles = new List<int>();
        List<Vector2> uVs = new List<Vector2>();

        // To keep track of added triangles
        int ti = 0;

        for (int triIndex = 0; triIndex < meshData.Triangles.Length; triIndex+=3)
        {
            int a = meshData.Triangles[triIndex];
            int b = meshData.Triangles[triIndex+1];
            int c = meshData.Triangles[triIndex+2];

            Vector3 v1 = meshData.Vertices[a];
            Vector3 v2 = meshData.Vertices[b];
            Vector3 v3 = meshData.Vertices[c];

            float h1 = v1.y;
            float h2 = v2.y;
            float h3 = v3.y;

            // Find min and max points
            float hMin = Mathf.Min(h1, h2, h3);
            float hMax = Mathf.Max(h1, h2, h3);

            // Floor min and max depending on distance between planes
            hMin += Mathf.Sign(-hMin) * (hMin % unitHeight);
            hMax += Mathf.Sign(-hMax) * (hMax % unitHeight);

            for (float h = hMin; h <= hMax; h+= unitHeight)
            {
                TriPointConfiguration pointConfiguration = TriPointConfiguration.NoneAbove;
                int pointCountAbove = 0;
                if (h1 >= h)
                {
                    pointConfiguration |= TriPointConfiguration.FirstAbove;
                    pointCountAbove++;
                }
                if (h2 >= h)
                {
                    pointConfiguration |= TriPointConfiguration.SecondAbove;
                    pointCountAbove++;
                }
                if (h3 >= h)
                {
                    pointConfiguration |= TriPointConfiguration.ThirdAbove;
                    pointCountAbove++;
                }

                Vector3 v1_Old = v1;
                Vector3 v2_Old = v2;
                Vector3 v3_Old = v3;
                Vector2 uv1_Old = meshData.UVs[a];
                Vector2 uv2_Old = meshData.UVs[b];
                Vector2 uv3_Old = meshData.UVs[c];

                Vector2 uv1 = meshData.UVs[a];
                Vector2 uv2 = meshData.UVs[b];
                Vector2 uv3 = meshData.UVs[c];

                switch (pointConfiguration)
                {
                    case TriPointConfiguration.FirstAbove:
                        // v1 v2 v3 = v2 v3 v1
                        v1 = v2_Old;
                        v2 = v3_Old;
                        v3 = v1_Old;
                        uv1 = uv2_Old;
                        uv2 = uv3_Old;
                        uv3 = uv1_Old;
                        break;
                    case TriPointConfiguration.SecondAbove:
                        // v1 v2 v3 = v3 v1 v2
                        v1 = v3_Old;
                        v2 = v1_Old;
                        v3 = v2_Old;
                        uv1 = uv3_Old;
                        uv2 = uv1_Old;
                        uv3 = uv2_Old;
                        break;
                    case TriPointConfiguration.ThirdAbove:
                        // Fine as is
                        break;
                    case TriPointConfiguration.FirstAbove | TriPointConfiguration.SecondAbove:
                        // Fine as is
                        break;
                    case TriPointConfiguration.SecondAbove | TriPointConfiguration.ThirdAbove:
                        // v1 v2 v3 = v2 v3 v1
                        v1 = v2_Old;
                        v2 = v3_Old;
                        v3 = v1_Old;
                        uv1 = uv2_Old;
                        uv2 = uv3_Old;
                        uv3 = uv1_Old;
                        break;
                    case TriPointConfiguration.FirstAbove | TriPointConfiguration.ThirdAbove:
                        // v1 v2 v3 = v3 v1 v2
                        v1 = v3_Old;
                        v2 = v1_Old;
                        v3 = v2_Old;
                        uv1 = uv3_Old;
                        uv2 = uv1_Old;
                        uv3 = uv2_Old;
                        break;
                    case TriPointConfiguration.FirstAbove | TriPointConfiguration.SecondAbove | TriPointConfiguration.ThirdAbove:
                        // Fine as is
                        break;
                    case TriPointConfiguration.NoneAbove:
                        Debug.LogError("All points are below the plane which shouldn't happen.");
                        break;
                }

                h1 = v1.y;
                h2 = v2.y;
                h3 = v3.y;

                // Current plane vertices
                Vector3 v1_c = new Vector3(v1.x, h, v1.z);
                Vector3 v2_c = new Vector3(v2.x, h, v2.z);
                Vector3 v3_c = new Vector3(v3.x, h, v3.z);

                // Vertices below for walls
                Vector3 v1_b = new Vector3(v1.x, h-unitHeight, v1.z);
                Vector3 v2_b = new Vector3(v2.x, h-unitHeight, v2.z);
                Vector3 v3_b = new Vector3(v3.x, h-unitHeight, v3.z);

                if (pointCountAbove == 3)
                {
                    // All points above, just add a single triangle
                    vertices.Add(v1_c);
                    uVs.Add(uv1);
                    vertices.Add(v2_c);
                    uVs.Add(uv2);
                    vertices.Add(v3_c);
                    uVs.Add(uv3);
                    triangles.Add(ti);
                    triangles.Add(ti + 1);
                    triangles.Add(ti + 2);

                    ti += 3;
                }
                else
                {
                    // Find points in between the planes (requires interpolation)
                    float t1 = (h1 - h) / (h1 - h3);
                    Vector3 v1_c_n = Vector3.Lerp(v1_c, v3_c, t1);
                    Vector3 v1_b_n = Vector3.Lerp(v1_b, v3_b, t1);

                    float t2 = (h2 - h) / (h2 - h3);
                    Vector3 v2_c_n = Vector3.Lerp(v2_c, v3_c, t2);
                    Vector3 v2_b_n = Vector3.Lerp(v2_b, v3_b, t2);  

                    if (pointCountAbove == 2)
                    {
                        // Add roof
                        vertices.Add(v1_c);
                        uVs.Add(uv1);
                        vertices.Add(v2_c);
                        uVs.Add(uv2);
                        vertices.Add(v2_c_n);
                        uVs.Add(uv2);
                        vertices.Add(v1_c_n);
                        uVs.Add(uv1);

                        triangles.Add(ti);
                        triangles.Add(ti + 1);
                        triangles.Add(ti + 2);
                        triangles.Add(ti+2);
                        triangles.Add(ti+3);
                        triangles.Add(ti);
                        ti += 4;

                        // Add wall
                        vertices.Add(v1_c_n);
                        uVs.Add(uv1);
                        vertices.Add(v2_c_n);
                        uVs.Add(uv2);
                        vertices.Add(v2_b_n);
                        uVs.Add(uv2);
                        vertices.Add(v1_b_n);
                        uVs.Add(uv1);
                        
                        triangles.Add(ti);
                        triangles.Add(ti + 1);
                        triangles.Add(ti + 2);
                        triangles.Add(ti);
                        triangles.Add(ti + 2);
                        triangles.Add(ti + 3);
                        ti += 4;
                    }
                    else if(pointCountAbove == 1)
                    {
                        // Add roof
                        vertices.Add(v3_c);
                        uVs.Add(uv3);
                        vertices.Add(v1_c_n);
                        uVs.Add(uv1);
                        vertices.Add(v2_c_n);
                        uVs.Add(uv2);
                        
                        triangles.Add(ti);
                        triangles.Add(ti + 1);
                        triangles.Add(ti + 2);
                        ti += 3;
                        
                        vertices.Add(v2_c_n);
                        uVs.Add(uv2);
                        vertices.Add(v1_c_n);
                        uVs.Add(uv1);
                        vertices.Add(v1_b_n);
                        uVs.Add(uv1);
                        vertices.Add(v2_b_n);
                        uVs.Add(uv2);
                        
                        triangles.Add(ti);
                        triangles.Add(ti + 1);
                        triangles.Add(ti + 3);
                        triangles.Add(ti + 1);
                        triangles.Add(ti + 2);
                        triangles.Add(ti + 3);
                        ti += 4;
                    }
                }
            }
        }
        
        return new MeshData(vertices.ToArray(), triangles.ToArray(), uVs.ToArray());
    }

    public static MeshData GenerateTerracedMeshData(float[,] heightMap, float heightCoefficient, float unitHeight, AnimationCurve coefficientOverHeight)
    {
        return GenerateTerracedMeshData(GenerateMeshData(heightMap, heightCoefficient, coefficientOverHeight), unitHeight);
    }

    [Flags]
    public enum TriPointConfiguration
    {
        NoneAbove = 0,
        FirstAbove = 1,
        SecondAbove = 2,
        ThirdAbove = 4
    }
}

public class MeshData
{
    public readonly Vector3[] Vertices;
    public readonly int[] Triangles;
    public readonly Vector2[] UVs;

    private int triangleIndex;

    public MeshData(Vector3[] vertices, int[] triangles, Vector2[] uVs)
    {
        Vertices = vertices;
        Triangles = triangles;
        UVs = uVs;
    }

    public MeshData(int meshLength, int meshWidth)
    {
        Vertices = new Vector3[meshLength * meshWidth];
        Triangles = new int[(meshLength - 1) * (meshWidth - 1) * 6];
        UVs = new Vector2[meshLength * meshWidth];
    }

    public void AddTriangle(int a, int b, int c)
    {
        Triangles[triangleIndex] = a;
        Triangles[triangleIndex + 1] = b;
        Triangles[triangleIndex + 2] = c;
        triangleIndex += 3;
    }

    public Mesh CreateMesh()
    {
        Mesh mesh = new Mesh
        {
            vertices = Vertices, 
            triangles = Triangles, 
            uv = UVs
        };

        mesh.RecalculateNormals();
        return mesh;
    }
}