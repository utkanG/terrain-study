using System;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public enum MeshMode
    {
        Default,
        Terraced
    }

    public MapChunk ChunkPrefab;

    [Header("Mesh Settings")]
    [Min(1)]
    public int MapWidth;
    [Min(1)]
    public int MapLength;
    public float HeightCoefficient = 1;
    public AnimationCurve MeshHeightCurve;
    public MeshMode meshMode;
    [Min(0.1f)]
    public float TerraceHeight = 1;
    [Min(1)]
    public int ChunkCount = 1;

    [Header("Noise Settings")]
    public float NoiseScale;
    [Min(0)]
    public int Octaves;
    [Range(0,1)]
    public float Persistence;
    [Min(1)]
    public float Lacunarity;
    public Vector2 Offset;
    public int Seed;

    public bool AutoUpdate;

    private List<MapChunk> mapChunks;

    public void GenerateMap()
    {
        ClearMap();

        for (int x = -(ChunkCount-1); x <= ChunkCount-1; x++)
        {
            for (int z = -(ChunkCount - 1); z <= ChunkCount - 1; z++)
            {
                MapChunk newChunk = Instantiate(ChunkPrefab, transform);
                Vector3 offsetPosition = new Vector3(x * (MapWidth - 1), 0f, z * (MapLength - 1));
                newChunk.transform.localPosition = offsetPosition;

                Vector2 offset = Offset + new Vector2(offsetPosition.x, -offsetPosition.z) / NoiseScale;
                float[,] noiseMap = Noise.GenerateNoiseMap(MapWidth, MapLength, Seed, NoiseScale, Octaves, Persistence, Lacunarity, offset);
                newChunk.DrawMesh(
                    meshMode == MeshMode.Default ? MeshGenerator.GenerateMeshData(noiseMap, HeightCoefficient, MeshHeightCurve) : MeshGenerator.GenerateTerracedMeshData(noiseMap, HeightCoefficient, TerraceHeight, MeshHeightCurve));

                mapChunks.Add(newChunk);
            }
        }
    }

    private void ClearMap()
    {
        mapChunks ??= new List<MapChunk>();

        foreach (MapChunk mapChunk in mapChunks)
        {
            if (mapChunk != null)
            {
                DestroyImmediate(mapChunk.gameObject);
            }
        }

        mapChunks.Clear();
    }
}
