using System;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class MapChunk : MonoBehaviour
{
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    private static readonly int s_BaseMap = Shader.PropertyToID("_BaseMap");

    public void DrawMesh(MeshData meshData)
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        meshFilter.sharedMesh = meshData.CreateMesh();
        //meshRenderer.sharedMaterial.SetTexture(s_BaseMap, texture);;
    }
}